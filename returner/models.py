from django.db import models
from datetime import datetime
from time import strftime

class Jids(models.Model):
    class Meta:
        db_table = 'jids'
    jid = models.CharField(max_length=225, blank=True, unique=True)
    load = models.TextField(blank=True)

    def __unicode__(self):
        return u'%s %s' % (self.jid, self.load)

class Salt_returns(models.Model):
    class Meta:
        db_table = 'salt_returns'
    fun = models.CharField(max_length=50, blank=True)
    jid = models.CharField(max_length=255, blank=True)
    returns = models.TextField(blank=True)
    minion_id = models.CharField(max_length=255, blank=True)
    success = models.CharField(max_length=10, blank=True)
    full_ret = models.TextField(blank=True)
    alter_time = models.DateTimeField(auto_created=True)

    def __unicode__(self):
        return u'%s %s %s %s %s %s %s' % (self.fun, self.jid, self.returns, self.minion_id, self.success, self.full_ret, self.alter_time)

class Salt_events(models.Model):
    class Meta:
        db_table = 'salt_events'
    tag = models.CharField(max_length=255, blank=True)
    data = models.TextField(blank=True)
    alter_time = models.DateTimeField(auto_created=True)
    minion_id = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return u'%s %s %s %s' % (self.tag, self.data, self.alter_time, self.minion_id)

class Salt_grains(models.Model):
    class Meta:
        db_table = 'salt_grains'
    minion_id = models.CharField(max_length=255, null=True, blank=True)
    grains = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return u'%s %s' % (self.minion_id, self.grains)

